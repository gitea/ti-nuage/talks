# Supports de discussions et conférences de Ti nuage

## Compilation des supports écris avec Beamer

Les `Makefile` fournis fonctionnent de la manière suivante.

- Un document par répertoire. Le répertoire est créé lors de la création du document. Le répertoire doit aussi contenir un fichier Makefile avec le contenu suivant, où `document.tex` est le nom du document créé.

        SOURCE = document.tex
        include ../Makefile.common

  Pour générer les fichiers de présentation, il suffit de se placer dans ce répertoire est d'entrer `make`.

- Un document est normalement associé à un événement, une date et un lieu particuliers. S'il est ré-employé pour un autre événement, un *patch* est utilisé pour créer une copie du document pour cet événement.

- Un *patch* permet d'amender le document pour modifier les références de lieu, de date, et éventuellement pour apporter des améliorations sur l'itération précédente. À un événement correspond un *patch*, et tous les *patches* s'appliquent sur le document original.

- En cas de doute, le répertoire `logiciels_libres` montre à quoi tout ceci doit ressembler.
