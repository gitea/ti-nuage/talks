% Rédigé par David Soulayrol, janvier 2023.
% Ce document peut être distribué selon les termes de la CC BY 4.0.

\documentclass[onlytextwidth]{beamer}

\mode<presentation> {
  \usetheme{EastLansing}
  \setbeameroption{hide notes}
}

\usepackage[frenchb]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphics}

\setbeamercolor{citation}{fg=MSUgreen,bg=white}

% TODO Add the license in the footer

\title{Logiciels libres et alternatifs}
\subtitle{Un tour d'horizon}
\author[D. Soulayrol]{David Soulayrol \\ \texttt{david@ti-nuage.fr}}
\institute[Ti Nuage]{Association Ti Nuage \\ \texttt{https://ti-nuage.fr}}
\date[Plougrescant 2023]{Faites du numérique, Plougrescant, 2023}

\pgfdeclareimage[height=0.5cm]{logo-tinuage}{../communs/logo-ti-nuage}
\logo{\pgfuseimage{logo-tinuage}}

\begin{document}

\begin{frame}
  \titlepage
  \note{L'objet de cette présentation est de fournir tout d'abord une introduction à la notion de logiciel libre, puis de fournir des exemples concrets d'usage montrant la variété des logiciels disponibles.

  Tous les logiciels mis en avant dans cette présentation, et une grande quantité des autres logiciels proposés sont multi-plateformes.}
\end{frame}

\input{../communs/presentation.tex}

\begin{frame}{Présentations}
  \begin{beamercolorbox}{citation}
  \textit{Toute technologie suffisamment avancée est indiscernable de la magie.}
  \end{beamercolorbox}
  \begin{flushright}
    — Arthur C. Clarke
  \end{flushright}
  \note{Il n'y a aucune magie dans l'informatique. Il n'y a aucune décision spontanée dans un logiciel. Un logiciel est déterministe dans le sens où il produira toujours ce pour quoi il a été écrit, selon les données qui lui sont fournies.}
\end{frame}

\section{Qu'est-ce que le logiciel libre ?}

\begin{frame}{Aperçu}
  \tableofcontents[currentsection]
\end{frame}

\subsection{Définition du logiciel}

\begin{frame}{Définition du logiciel}
  \begin{beamercolorbox}{citation}
    Un logiciel est un ensemble de séquences d’instructions interprétables par une machine et d’un jeu de données nécessaires à ces opérations.
  \end{beamercolorbox}
  \begin{flushright}
    — Wikipedia, le 21/01/2023
  \end{flushright}
  \vspace{1em}
  \begin{itemize}
    \item Synonymes : application, programme
    \item En anglais : \textit{software} (de \textit{hardware})
  \end{itemize}
  \note{
    \begin{itemize}
      \item Un ordinateur est une machine extrêmement performante pour le traitement des nombres. Elle ne connaît que des instructions élémentaires comme additioner deux nombres, les enregistrer à tel emplacement, les transformer de telle manière, etc. C'est l'accumulation très rapide de ces opérations qui permet d'obtenir des fonctions plus complexes.

      \item Un jeu de données est un ensemble de valeurs contextualisées. Par exemple, le jeu de donnés nécessaire à une succession d'opérations, ce sont des nombres. Le jeu de données qui caractérise une personne sur un site marchand peut être constitué de son adresse, ses coordonnées bancaires, son âge, ses préférences, etc.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Typologie des logiciels}
  Il est possible de classer les logiciels selon quantités de critères, tels que :
  \vspace{.5em}
  \begin{itemize}
    \item leur rôle : logiciel système, logiciel applicatif ;
    \item leur environnement : programme natif, application Web, logiciel embarqué ;
    \item la license accordée : logiciel libre, gratuitiel, partagiciel, logiciel privateur.
  \end{itemize}
  \note{Le premier logiciel à s'exécuter sur un ordinateur est ce que l'on appelle son code d'amorçage (le BIOS, par exemple). Celui-ci démarre ensuite le système d'exploitation, qui est un ensemble de logiciels destiné à prendre en charge le matériel et à organiser l'exécution d'autres logiciels, plus utiles à l'utilisateur.

  Un logiciel qui s'exécute sur le système d'exploitation est dit \textit{natif}. Un logiciel qui s'exécute dans le cadre plus strict d'un programme tiers est dit \textit{interprété} ; les scripts d'un site Web par exemple, ou bien un programme écrit dans Scratch.}
\end{frame}

\begin{frame}{Et les algorithmes ?}
  \begin{beamercolorbox}{citation}
    Un algorithme est une suite finie et non ambiguë d'instructions et d’opérations permettant de résoudre une classe de problèmes.
  \end{beamercolorbox}
  \begin{flushright}
    — Wikipedia, le 21/01/2023
  \end{flushright}
  \vspace{1em}
  \begin{itemize}
    \item Un algorithme est une recette.
    \item Par abus de langage, on appelle \textit{algorithme} les choix faits par un développeur.
  \end{itemize}
  \note{
    \begin{itemize}
      \item L'analogie courante pour expliquer un algorithme est le décrire comme une recette. Comme dans une recette, chaque étape consiste en une opération clairement définie et donne un nouveau résultat.

      Par exemple, un traitement de texte utilise un algorithme pour construire automatiquement la table des matières d'un document. Il en exploite un autre lorsque l'utilisateur fait une recherche dans le document. Mais aussi pour afficher l'aider, gérer l'enregistrement d'un fichier, modifier le zomm du document, etc. De manière générale, toute fonction d'un logiciel est réalisée à l'aide d'un algorithme.

      \item Il faut toujours se souvenir que derrière un algorithme, il y a un choix humain. Par exemple, la manière dont Parcoursup retient les choix d'un candidat a été choisie et validée par l'État. La manière dont les articles sont affichés sur une parge du site Facebook est clairement définie par cette société.
    \end{itemize}}
\end{frame}

\subsection{Le logiciel libre}

\begin{frame}{Le logiciel libre}
  \begin{itemize}
    \item Le logiciel libre est un mouvement social initié par Richard M. Stallman au début des années 80.
    \item Il repose sur les principes de Liberté, d'Égalité, et de Fraternité.
  \end{itemize}
  \note{Aux débuts de l'informatique, le logiciel était naturellement partagé entre les équipes de programmeurs ; c'est le matériel qui constituait la source de revenus. Dans le courant des années 70, en particulier avec la naissance de la micro-informatique, le logiciel devient une valeur distincte, et commence a être protégé.

  Un logiciel non libre est parfois appelé \textit{propriétaire} (ce qui n'a pas de sens) ou \textit{privateur}.

  La devise française est couramment citée par RMS lorsqu'il intervient pour une conférence en France. La liberté parse que l'utilisateur est libre de faire ce qu'il veut du logiciel, l'égalité parce que cette liberté s'applique à tous, la fraternité parce que la notion de partage est au cœur du mouvement.}
\end{frame}

\begin{frame}{Quatre libertés...}
  \begin{enumerate}
    \item[0]<1-> La liberté d’utiliser le logiciel, pour quelque usage que ce soit.
    \item[1]<2-> La liberté d’étudier le fonctionnement du programme, et de l’adapter à vos propres besoins.
    \item[2]<3-> La liberté de redistribuer des copies à tout le monde.
    \item[3]<4-> La liberté d’améliorer le programme et de publier vos améliorations.
  \end{enumerate}
\end{frame}

\begin{frame}{... Définies par une licence}
  \begin{itemize}
    \item Tous les logiciels sont soumis au droit d'auteur. Ce droit est exercé par le biais d'une licence qui énumère les droits que l'auteur choisit d'octroyer à l'utilisateur.
    \item Une licence libre garantit les quatre libertés de l'utilisateur.
  \end{itemize}
  \vspace{1em}
  Exemples : les licences BSD (\textit{Berkeley Software Distribution License}), GNU GPL (\textit{General Public License}), \textit{Mozilla Public License}, \textit{MIT License}\ldots
  \note[item]{Sauf si un logiciel est explicitement abandonné au domaine public.}
\end{frame}

\begin{frame}{Logiciel libre et Open Source}
  \begin{itemize}
    \item Le terme \textit{open source} provient de l'ouvrage \textit{La Cathédrale et le Bazar} de Eric S. Raymond.
    \item Il s'agit d'une manière de qualifier certaines méthodes de développement exploitant la réutilisation du code source.
  \end{itemize}
  \note{Même si les licences utilisées dans des produits qualifiés d'open source accordent très souvent les même droits aux utilisateur, ce terme s'attache à mettre en valeur la disponibilité du code plutôt que la liberté. Il est davantage soutenu par les industries de l'informatique.

  Dans la pratique, les termes se trouvent de manière interchangeable.}
\end{frame}

\section{Les usages}

\begin{frame}{Aperçu}
  \tableofcontents[currentsection]
\end{frame}

\subsection{La bureautique}

\begin{frame}{Pour naviguer sur Internet}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Mozilla Firefox}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_firefox_2019}
      \par
      \vspace{1em}
      {\scriptsize https://www.mozilla.org}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{Tor Browser} \hfill \\
            {\scriptsize https://www.torproject.org/fr/} \\
            {\small Un navigateur qui exploite le projet Tor.}
      \item \textbf{Chromium} \hfill \\
            {\scriptsize https://www.chromium.org/Home/} \\
            {\small Le projet qui sert de support à Google Chrome ou Microsoft Edge.}
      \item \textbf{Konqueror}, \textbf{Pale Moon}, \textbf{Midori}, \ldots
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{À propos des navigateurs}
  \begin{itemize}
    \item Le moteur de rendu d'un navigateur est aujourd'hui extrêmement complexe.
    \item Beaucoup de navigateurs partagent le même moteur. Il n'existe donc en réalité
          qu'une très faible diversité.
  \end{itemize}
  \vspace{1em}
  \begin{description}
    \item[Gecko] Firefox, SeaMonkey, K-Meleon, Camino, Galeon, Epiphany, \ldots
    \item[Blink] Google Chrome, Chromium, Brave, Microsoft Edge, Opera, Vivaldi, Yandex Browser, \ldots
    \item[Webkit] Safari, Konqueror, Midori, Falcon, Uzbl, \ldots
  \end{description}
  \note{Le moteur d'un navigateur Web est la partie qui gère l'affichage des pages et l'interprétation de leur comportement dynamique, c'est à dire l'exécution des scripts qui y sont attachés.

  Le rendu d'une page HTML est complexe parce qu'il doit suivre un ensemble de directives de style et s'adapter au format d'affichage. La multiplication de directives régissant ces styles, et les interférences de ces règles entre elles rend cette tâche extrêmement ardue.

  Gecko est issu de Mozilla. Blink de Google. Webkit est une évolution, par Apple, de KHTML du projet KDE.}
\end{frame}

\begin{frame}{Pour gérer mes courriels}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Mozilla Thunderbird}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_thunderbird_2018}
      \par
      \vspace{1em}
      {\scriptsize https://www.thunderbird.net/fr/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{Claws Mail} \hfill \\
            {\scriptsize https://www.claws-mail.org/}
            {\small Un logiciel à l'interface plus datée, mais mûr et efficace.}
      \item \textbf{K-9 Mail} \hfill \\
            {\scriptsize https://k9mail.app/} \\
            {\small Un client de courriel pour Android.}
      \item \textbf{FairEmail} \hfill \\
            {\scriptsize https://email.faircode.eu/} \\
            {\small Un autre logiciel pour Android, orienté vers la sécurité.}
      \item \textbf{Evolution}, \textbf{Geary}, \textbf{KMail}, \ldots
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Les courriels dans le navigateur}
  \begin{itemize}
    \item Il est de plus en plus fréquent de gérer ses courriels avec une interface Web.
    \item Les avantages sont la portabilité, la disponibilité.
    \item Inconvénients : le manque de fonctionnalités avancées, la fragilité dûe à l'environnement du navigateur.
  \end{itemize}
  \vspace{1em}
  Exemples : Roundcube, Zimbra, NextCloud Mail, Rainloop, \ldots
\end{frame}

\begin{frame}{Pour discuter en ligne}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Pidgin}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_pidgin}
      \par
      \vspace{1em}
      {\scriptsize https://pidgin.im/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{Gajim} \hfill \\
            {\scriptsize https://gajim.org/} \\
            {\small Un client XMPP complet.}
      \item \textbf{Conversations} \hfill \\
            {\scriptsize https://conversations.im/} \\
            {\small Un client XMPP pour Android.}
      \item \textbf{Signal}, \textbf{Element}, \textbf{Dino}, \ldots
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Les protocoles de discussion}
  \begin{itemize}
    \item Les logiciels de discussion en ligne reposent sur un ou plusieurs protocoles.
    \item Ces protocoles permettent les messages textuels, mais aussi la voix, les échanges de fichiers, \ldots
    \item La license de ces protocoles est aussi importante que celle du logiciel.
  \end{itemize}
  \vspace{1em}
  \begin{description}
    \item[Matrix] https://matrix.org/
    \item[XMPP] https://xmpp.org/
  \end{description}
\end{frame}

\begin{frame}{Audio et Vidéo-conférence}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Jitsi Meet}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_jitsi}
      \par
      \vspace{1em}
      {\scriptsize https://jitsi.org/jitsi-meet/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{BigBlueButton} \hfill \\
            {\scriptsize https://bigbluebutton.org} \\
            {\small Une solution complète avec support de tableau blanc.}
      \item \textbf{Mumble} \hfill \\
            {\scriptsize https://www.mumble.info} \\
            {\small Un logiciel d'audio-conférence.}
      \item \textbf{Jami} \hfill \\
            {\scriptsize https://jami.net} \\
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Pour rédiger un document}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Libre Office Writer}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_libreoffice}
      \par
      \vspace{1em}
      {\scriptsize https://fr.libreoffice.org/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{OpenOffice} \hfill \\
            {\scriptsize https://www.openoffice.org/} \\
            {\small Un cousin éloigné de Libre Office.}
      \item \textbf{Only Office} \hfill \\
            {\scriptsize https://www.onlyoffice.com/fr/} \\
            {\small Un suite bureautique dans le navigateur.}
      \item \textbf{Abiword} \hfill \\
            {\scriptsize http://www.abisource.com/} \\
            {\small Un logiciel plus léger dédié au texte.}
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Différentes manières d'éditer du texte}
  \begin{itemize}
    \item Les traitements de texte comme Writer servent à mettre en forme un document textuel.
    \item Les éditeurs permettent l'édition d'un texte brute : Emacs, Vim, Geany, Kate, GEdit, Notepad++, \ldots
    \item Des outils en ligne permettent la rédaction collaborative : Etherpad
    \item Il existe d'autres moyens de transformer du texte :
          \begin{itemize}
            \item Les processeurs de text : Groff, \LaTeX
            \item Les systèmes de production d'information : Scenari
          \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Les activités artistiques}

\begin{frame}{Pour dessiner}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Gimp}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_gimp}
      \par
      \vspace{1em}
      {\scriptsize https://www.gimp.org/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{Krita} \hfill \\
            {\scriptsize https://krita.org/} \\
            {\small Un logiciel de peinture numérique.}
      \item \textbf{MyPaint} \hfill \\
            {\scriptsize http://mypaint.org/} \\
            {\small Un autre logiciel de peinture.}
      \item \textbf{Inkscape} \hfill \\
            {\scriptsize https://inkscape.org/fr/} \\
            {\small Un outil de dessin vectoriel.}
    \end{itemize}
  \end{columns}
\end{frame}

\subsection{Les loisirs}

\begin{frame}{Pour jouer de la musique}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{Clementine}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_clementine}
      \par
      \vspace{1em}
      {\scriptsize https://www.clementine-player.org/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{Strawberry} \hfill \\
            {\scriptsize https://www.strawberrymusicplayer.org/} \\
            {\small Un cousin de Clementine.}
      \item \textbf{Audacious} \hfill \\
            {\scriptsize https://audacious-media-player.org/} \\
            {\small Un descendant de XMMS.}
      \item \textbf{Rhythmbox}, \textbf{Amarok}, \ldots
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Pour regarder un film}
  \begin{columns}[c]
    \column{40mm}
    \begin{center}
      \textbf{VLC}
      \par
      \vspace{1em}
      \includegraphics[width=20mm]{images/logo_vlc}
      \par
      \vspace{1em}
      {\scriptsize https://www.videolan.org/vlc/}
    \end{center}
    \column{\dimexpr\linewidth-40mm-5mm}
    \begin{itemize}
      \item \textbf{MPV} \hfill \\
            {\scriptsize https://mpv.io/}
      \item \textbf{MPlayer} \hfill \\
            {\scriptsize https://mplayerhq.hu/}
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Pour jouer}
  \begin{columns}[c]
    \column{\dimexpr\linewidth/2}
    \begin{itemize}
      \item \textbf{0AD} \hfill \\
            {\scriptsize https://play0ad.com/} \\
            {\small Un jeu de stratégie temps-réel (RTS) historique.}
      \item \textbf{Battle for Wesnoth} \hfill \\
            {\scriptsize https://www.wesnoth.org/} \\
            {\small Un jeu de stratégie par tours médiéval-fantastique.}
      \item \textbf{Minestest} \hfill \\
            {\scriptsize https://www.minetest.net/} \\
            {\small Un univers de création à base de \textit{voxels}.}
    \end{itemize}
    \column{\dimexpr\linewidth/2}
    \begin{itemize}
      \item \textbf{Shattered Pixel Dungeon} \hfill \\
            {\scriptsize http://shatteredpixel.com/shatteredpd/} \\
            {\small Un jeu d'exploration de donjon.}
      \item \textbf{Endless Sky} \hfill \\
            {\scriptsize https://endless-sky.github.io/} \\
            {\small Un jeu de commerce et de combats spatial.}
      \item \textbf{Unvanquished} \hfill \\
            {\scriptsize https://unvanquished.net/} \\
            {\small Un \textit{first-person shooter} doublé de stratégie.}
    \end{itemize}
  \end{columns}
\end{frame}

\subsection{Exploiter l'ordinateur}

\begin{frame}{Le système d'exploitation}
  \begin{itemize}
    \item Le système d'exploitation est un ensemble de logiciels qui permet l'accès et l'utilisation des ressources de la machine pour l'utilisateur et les autres programmes.
    \item Le temps passé sur l'ordinateur tient principalement à l'usage d'applications spécifiques pour le travail, les loisirs, \ldots.
    \item L'utilisation de logiciels libres et multi-plateformes donne aussi le choix du système d'exploitation.
  \end{itemize}
\end{frame}

\begin{frame}{Changer le système !}
  De nombreux systèmes d'exploitations libres sont disponibles.
  \vspace{1em}
  \begin{description}
    \item[GNU/Linux] Un descendant de UNIX, disponible sour la forme de nombreuses \textit{distributions} : Debian, Ubuntu, RedHat, \ldots
    \item[Free BSD] Un autre système de la famille UNIX.
    \item[Haiku] Un système inspiré de BeOS.
    \item[FreeDOS] Une implémentation libre du DOS des premiers PC.
    \item[ReactOS] Une implémentation libre de Microsoft Windows.
  \end{description}
\end{frame}

\end{document}
