#!/usr/bin/env bash

REAL_COMMAND=$(readlink -f "$0")

cd "$(dirname "$REAL_COMMAND")" || exit 1

find . -type f -name Makefile -exec dirname -z "{}" \; \
     | sort -zu \
     | sed -z 's/$/\n/' \
     | while read -r d; do cd "$d"; make all; cd -; done
