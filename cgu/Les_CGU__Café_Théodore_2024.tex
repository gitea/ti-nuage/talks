% Rédigé par David Soulayrol, septembre 2023.
% Ce document peut être distribué selon les termes de la CC BY 4.0.

\documentclass[onlytextwidth]{beamer}

\mode<presentation> {
  \usetheme{EastLansing}
  \setbeameroption{hide notes}
}

\usepackage[frenchb]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphics}

\setbeamercolor{citation}{fg=MSUgreen,bg=white}

% TODO Add the license in the footer

\title{De la confiance}
\subtitle{TOS, Privacy et autre barbarismes}
\author[D. Soulayrol]{David Soulayrol \\ \texttt{david@ti-nuage.fr}}
\institute[Ti Nuage]{Association Ti Nuage \\ \texttt{https://ti-nuage.fr}}
\date[Café Théodore 2024]{Café Théodore, Trédrez-Locquémeau, 2024}

\pgfdeclareimage[height=0.5cm]{logo-tinuage}{../communs/logo-ti-nuage}
\logo{\pgfuseimage{logo-tinuage}}

\begin{document}

\begin{frame}
  \titlepage
  \note{L'idée de cette présentation est née d'une part de la volonté de mettre en lumière ce que coûte réellement un service numérique « gratuit », et d'autre part de répondre à la question entendue dans un GULL : « comment est-ce que je peux faire confiance à un fournisseur de service qui se prétend plus respectueux ? »}
\end{frame}

\input{../communs/presentation.tex}

\begin{frame}{Introduction}
  \begin{beamercolorbox}{citation}
  \textit{Microsoft est-il le Grand Satan ?}
  \end{beamercolorbox}
  \begin{flushright}
    — \texttt{\small https://www.gnu.org/philosophy/microsoft.fr.html}
  \end{flushright}
  \vspace{1em}
  \begin{beamercolorbox}{citation}
    \textit{Don't be evil}
  \end{beamercolorbox}
  \begin{flushright}
    — Motto de Google jusqu'en 2018
  \end{flushright}
  \note{
    \begin{itemize}
      \item Identifier Microsoft à Satan nous vient d'une période durant laquelle la guerre des navigateurs faisait rage, et Microsoft comparait Linux à un cancer. Cette attitude est encore véhiculée par certains, de manière atavique et sans réflexion approfondie semble-t-il.
      \item D'autres, ou les mêmes, ont pu louer Google pour tous les services qu'il a offert à ses utilisateurs. Ou au contraire vénérer Apple qui se targue de mieux protéger les siens.
      \item L'objet d'une entreprise est toujours de gagner de l'argent, et elle agit toujours en fonction de ses intérêts. Il ne s'agit donc pas ici de vouer aux gémonies ou d'aduler telle ou telle entreprise, mais de juger leur comportement dans le cadre des services qu'elles proposent.
    \end{itemize}
  }
\end{frame}

\begin{frame}{De quoi parle-t-on ?}
  Cette présentation se concentre sur les structures qui offrent un service à destination d'un utilisateur. Ces services, gratuits ou non, comprennent :
  \begin{itemize}
    \item des outils logiciels (Apple, Adobe, Google, Microsoft, etc.) ;
    \item des ressources matérielles ou logiques (Les mêmes, Amazon, etc.) ;
    \item du « contenu » (Netflix, OpenIA, OpenStreetMap, Spotify, Wikipedia, etc.).
  \end{itemize}
  \pause
  \vspace{1em}
  Quelques exemples de services ignorés ici :
  \begin{itemize}
    \item les services d'identité numérique ;
    \item les registres de noms de domaine ;
    \item les plateformes de transport de courriel.
  \end{itemize}
  \note{
    \begin{itemize}
      \item Notons que le terme « contenu » est véhiculé par les entreprises, réduisant sa signification à une marchandise.
      \item S'il existe des fournisseurs de services attachés à un média en particulier, il s'avère que l'essentiel des entreprises de ce secteur possèdent un intérêt dans plusieurs domaines. Parce que le service en lui-même n'a d'autres intérêt pour elles que de monopoliser davantage l'attention de leurs utilisateurs.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Le coût d'un service numérique}
  Un service numérique suppose :
  \begin{itemize}
    \item du stockage ;
    \item de la puissance de calcul ;
    \item de la \textit{bande-passante} et de la \textit{disponibilité}.
  \end{itemize}
  \pause
  \vspace{1em}
  Il coûte :
  \begin{itemize}
    \item des achats, de la maintenance de matériel ;
    \item de l'énergie ;
    \item de la main d'œuvre ;
    \item des frais de fonctionnement.
  \end{itemize}
  \note{
    \begin{itemize}
      \item Comme le laisse entendre l'adage « si c'est gratuit, c'est vous le produit », un service n'est jamais gratuit. La structure qui fournit le service obtient toujours quelque chose en retour : paiement direct, subventions, dons, recettes publicitaires, etc. Souvent, les sources des recettes sont multiples.
      \item Mais parce que les services numériques sont le plus souvent proposés par de grandes entreprises, les frais comprennent la communication, les dividendes des actionnaires, les investissements pour leur croissance, etc.
    \end{itemize}
  }
\end{frame}

\section{Des relations contractuelles}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Les contrats}
  Du moment qu'il y a échange de services ou de biens, il y a nécessité de protéger chacune des parties et donc d'encadrer l'échange.

  \vspace{2em}
  \begin{beamercolorbox}{citation}
    En droit, un contrat est un accord de volontés concordantes (consentement) entre une ou plusieurs personnes (les parties) en vue de créer une ou des obligations juridiques.
  \end{beamercolorbox}
  \begin{flushright}
    — Wikipedia, le 24/09/2023
  \end{flushright}
  \note{
    \begin{itemize}
      \item Avertissement : je ne suis pas juriste, et cette entrée en matière n'a donc d'autre objet que de rappeler les bases qui constituent un contrat.
      \item Il est important en premier lieu de se souvenir que le contrat nécessite avant tout un consentement mutuel. Il est aussi intéressant de noter qu'il permet de protéger la plus faible des deux parties en présence.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Les CGU}
  Un contrat :
  \begin{itemize}
    \item détaille les droits et obligations de deux personnes dans le cadre d'un échange de biens ou de services ;
    \item permet de recourir à la justice pour forcer l'exécution des obligations.
  \end{itemize}
  \pause
  \vspace{1em}
  Le contrat entre un fournisseur de service et ses utilisateurs s'appelle les \textit{Conditions Générales d'Utilisation} (CGU). Il s'agit d'un \textit{contrat d'adhésion} :
  \begin{itemize}
    \item il n'y a pas de négociation possible, l'acceptation est en bloc ;
    \item il est souvent particulièrement long et peu compréhensible.
  \end{itemize}
  \note{
    \begin{itemize}
      \item Les CGU en anglais se nomment \textit{Terms of Service} (TOS).
      \item Les sujets abordés sont typiquement le rappel des conditions légales, la responsabilité des deux parties, la résolution des litiges.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Les données personnelles}
  Les conditions d'utilisation mettent le plus souvent en jeu des données à caractère confidentiel. Les CGU s'accompagnent donc en général d'un second document courament nommé \textit{Règles de confidentialité}, \textit{Vie privée} ou \textit{Données personnelles}.

  \vspace{1em}
  En Europe, l'échange de ces données est encadré par le \textit{Règlement Général de Protection des Données} (RGPD).
  \note{
    \begin{itemize}
      \item Noter le merveilleux oxymore \textit{Vie privée}.
      \item En anglais on rencontre en général \textit{Privacy} ou \textit{Data Privacy}.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Le RGPD}
  Les grands principes du RGPD sont :
  \begin{itemize}
    \item collecter seulement les données nécessaires ;
    \item être transparent sur leur collecte et leur utilisation ;
    \item faciliter l'exercice des droits des personnes ;
    \item fixer une durée de conservation des données ;
    \item les sécuriser ;
    \item s'inscrire dans une démarche continue de mise en conformité.
  \end{itemize}
  \vspace{1em}
  \url{https://www.cnil.fr/fr/reglement-europeen-protection-donnees}
  \note{
    \begin{itemize}
      \item Il est important de retenir que le but de la législation est d'obtenir de l'utilisateur un consentement éclairé. Il doit connaître quelles sont les données collectées, conservées, et pour quel usage.
      \item Le RGPD est donc la raison de la multiplication des fenêtres de dialogue demandant l'autorisation pour les cookies. Ces fenêtres ne sont pas une règle à suivre, leur omniprésence est une conséquence et le symptome du modèle économique répandu partout sur Internet.
    \end{itemize}
  }
\end{frame}

\section{Cas d'usage}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Une analyse sommaire}
  Les founisseurs, dans leurs CGU :
  \begin{itemize}
    \item se dédouannent de toute responsabilité en cas de problèmes ;
    \item écrivent des conditions sur l'âge ou la nationalité (souvent) ;
    \item imposent d'abandonner ses droits en cas de conflit (parfois).
  \end{itemize}
  \vspace{1em}
  Les analyses ci-après considèrent plus particulièrement le cas des données personnelles.
  Les documents cités ont été visités entre le 26 novembre et le 30 novembre 2023.

  \vspace{1em}
  Pour aller plus loin :
  \textit{Terms of Service, Didn't Read}.

  \vspace{.5em}
  \begin{center}
    \url{https://tosdr.org}
  \end{center}
  \note{
    \begin{itemize}
      \item Ceci n'est qu'un aperçu destiné à montrer un éventail des pratiques existantes.
      \item \scriptsize{Exemple d'impact de CGU : « TikTok added a section to its terms that mandates that all legal complaints be filed within one year of any alleged harm caused by using the app. The terms now say that TikTok users "forever waive" rights to pursue any older claims. And unlike a prior version of TikTok's terms of service archived in May 2023, users do not seem to have any options to opt out of waiving their rights. » \url{https://arstechnica.com/tech-policy/2023/12/tiktok-requires-users-to-forever-waive-rights-to-sue-over-past-harms/}}
      \item \scriptsize{Exemple d'abus : « Google is dealing with its second "lost data" fiasco in the past few months. [...] Google locked the issue thread on the Drive Community Forums at 170 replies before it was clear the problem was solved. It's also marking any additional threads as "duplicates" and locking them. » \url{https://arstechnica.com/gadgets/2023/12/google-calls-drive-data-loss-fixed-locks-forum-threads-saying-otherwise/}}
    \end{itemize}
}
\end{frame}

\subsection{Disney+}

\begin{frame}{Le cas Disney+}
  \begin{center}
    \url{https://www.disneyplus.com/fr-fr/legal/privacy-policy}
  \end{center}
  \vspace{.5em}
  \begin{center}
    \includegraphics[width=100mm]{images/disney_terms}
  \end{center}
  \note{
    \begin{itemize}
      \item Débuter par Disney+ permet de se rendre compte d'emblée que les problèmes liés à la protection des données personnelles ne concerne pas uniquement les services apparemment gratuits.
      \item Disney+ présente ses conditions sous une forme très traditionnelle, ce qui n'est pas le cas d'autres services comme on va le voir ensuite.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Types de renseignements recueillis}
  \pause
  \begin{itemize}
    \item Les données d'inscription.
    \item Les données de transaction.
    \item Les données partagées via des forums publics, ou sur les sites et applications Disney.
    \item Les données envoyées via des messageries ou applications Disney.
    \item Les données obtenues par un tiers.
    \item Les données de géo-localisation.
    \item Les données concernant l'utilisation des sites et des applications Disney+.
    \item Les données techniques des appareils utilisés et de leur environnement.
    \item Les images et vidéos prises en magasin.
    \item Les enregistrements d'appel.
  \end{itemize}
  \note{
    \begin{itemize}
      \item Une pause : À votre avis, quel type de renseignement trouve-t-on ?
      \item Les transactions sont les informations fournies avec le service clients, pour un achat, un échange, etc.
      \item Les messageries peuvent être des messages directs, en groupe, chats, etc.
      \item Considérant la technique et la géolocalisation, sont considérées par exemple l'adresse IP, le type et la configuration de l'appareil et du logiciel utilisé, les données fournies par le téléphone mobile.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Modalités de collecte}
  \begin{itemize}
    \item Saisie directe.
    \item Collecte durant l'usage des services et applications, que l'on soit connecté ou non.
    \item Outils d'analyse.
    \item Rapprochement des données de tiers.
  \end{itemize}
  \note{
    \begin{itemize}
      \item La saisie directe comprend les formulaires, les enquêtes, les visites dans un établissement, les interactions avec le service clients, etc.
      \item La collecte couvre les cookies, les pixels, les balises, utilisées sur les sites et les applications.
      \item L'analyse s'applique sur les sites, les applications, et sur les hébergements tiers.
      \item Données de tiers : le seul exemple fourni est la vérification de l'adresse à l'aide d'un service tiers. Ce n'est qu'un exemple...
    \end{itemize}
  }
\end{frame}

\begin{frame}{Utilisation des données}
  \begin{itemize}
    \item Fourniture des services vus, réservés, achetés.
    \item Communication à propos du compte et des transactions.
    \item Envoi d'offres commerciales Disney et de tiers.
    \item Personnalisation du service.
    \item Publicités ciblées selon l'activité chez Disney et chez des tiers.
    \item Développement et optimisation des services.
    \item Détection des opérations frauduleuses ou illégales.
    \item Communication au sujet de rappels de produits ou de problèmes de sécurité.
  \end{itemize}
  \pause
  \vspace{1em}
  Il ne s'agit là que de l'utilisation des données au sein du groupe Disney.
\end{frame}

\begin{frame}{Partage des données}
  Disney ne partage pas les données collectées... Sauf :
  \begin{itemize}
    \item si vous l'autorisez afin de recevoir des offres commerciales ;
    \item si vous l'autorisez pour certains services délivrés par des tiers : voyages, restaurants, plateformes de réseaux sociaux ;
    \item quand Disney coopère avec des établissements financiers (carte Visa Disney) ;
    \item lorsque vous fournissez des donnés à National Geographic ou Hulu ;
    \item lorsque des sociétés réalisent des services au nom de Disney ;
    \item dans le cas de vente d'entreprise, ou pour se confirmer au droit applicable.
  \end{itemize}
  \vspace{1em}
  Il est précisé que les données sont transférables partout dans le monde.
\end{frame}

\subsection{Google}

\begin{frame}{Le cas Google}
  \begin{center}
    \url{https://policies.google.com}
  \end{center}
  \vspace{.5em}
  \begin{center}
    \includegraphics[width=100mm]{images/google_terms}
  \end{center}
  \vspace{1em}
  \note{
    \begin{itemize}
      \item Les termes du contrat ont découpés en sections et couvrent plusieurs (longues) pages.
      \item La forme se fait plus aérée. On va voir qu'il s'agit de rassurer l'utilisateur, de se montrer « concerné » ou « pro-actif » concernant les données de l'utilisateur.
      \item Chaque rubrique est introduite par une vidéo sur... Youtube !
    \end{itemize}
  }
\end{frame}

\begin{frame}{Les conditions}
  Les conditions de Google couvrent l'usage :
  \vspace{.5em}
  \begin{itemize}
    \item des applications et des sites ;
    \item des plates-formes (comme Google Shopping, Firebase) ;
    \item des services incorporés par des applications ou des entreprises tierces ;
    \item des appareils.
  \end{itemize}
\end{frame}

\begin{frame}{Les accroches des premiers chapitres}
  \begin{beamercolorbox}{citation}
    Lorsque vous utilisez nos services, vous nous faites confiance pour le traitement de vos informations. Nous savons qu'il s'agit d'une lourde responsabilité, c'est pourquoi nous nous efforçons de les protéger, tout en vous permettant d'en garder le contrôle.
  \end{beamercolorbox}
  \begin{flushright}
    — Introduction aux règles de confidentialité
  \end{flushright}
  \vspace{1em}
  \begin{beamercolorbox}{citation}
    Nous voulons que vous compreniez le type d'informations que nous collectons via nos services.
  \end{beamercolorbox}
  \begin{flushright}
    — Chapitre « Informations collectées par Google »
  \end{flushright}
  \vspace{1em}
  \begin{beamercolorbox}{citation}
    Les données nous permettent de concevoir des services de meilleure qualité.
  \end{beamercolorbox}
  \begin{flushright}
    — Chapitre « Raisons pour lesquelles nous collectons vos données »
  \end{flushright}
\end{frame}

\begin{frame}{Les informations collectées}
    Explication des usages :

    \begin{itemize}
      \item Fournir nos services ;
      \item Assurer et améliorer nos services ;
      \item Développer de nouveaux services ;
      \item Proposer des services personnalisés, notamment en matière de contenu et d'annonces ;
      \item Évaluer les performances ;
      \item Communiquer avec vous ;
      \item Protéger Google, nos utilisateurs et le public.
      \end{itemize}
\end{frame}

\begin{frame}{Éléments explicitement créés ou fournis}
  \begin{itemize}
    \item<1-> des informations personnelles (nom, adresses, téléphone, etc.) ;
    \item<1-> des informations de paiement ;
    \item<2-> oh, et puis : \\
          \vspace{1em}
          \begin{beamercolorbox}{citation}
            Nous collectons également le contenu que vous créez, importez ou recevez de la part d'autres personnes via nos services. Cela inclut par exemple les e-mails que vous écrivez ou recevez, les photos et vidéos que vous enregistrez, les documents et feuilles de calcul que vous créez, ainsi que les commentaires que vous rédigez sur YouTube.
          \end{beamercolorbox}
          \begin{flushright}
            — Chapitre « Informations collectées par Google »
          \end{flushright}
  \end{itemize}
\end{frame}

\begin{frame}{Informations collectées 1/2}
  \begin{itemize}
    \item \textit{Vos application, navigateurs et appareils} ; \\
    \vspace{.5em}
    \begin{columns}[t]
      \begin{column}{0.05\textwidth}
        % Cette colonne est une marge
      \end{column}
      \begin{column}{0.45\textwidth}
        \footnotesize
        les identifiants uniques, \\
        le type et les paramètres du navigateur, \\
        le type et les paramètres de l'appareil, \\
        le système d'exploitation, \\
        des données relatives au réseau mobile, \\
      \end{column}
      \begin{column}{0.45\textwidth}
        \footnotesize
        le numéro de version de l'application, \\
        l'adresse IP, \\
        les rapports d'erreur, \\
        l'activité du système, \\
        la date, l'heure et l'URL de provenance de votre demande.
      \end{column}
    \end{columns}
    \vspace{1em}
    \item \textit{votre activité} ;
          \begin{itemize}
            \item ...
            \item \textit{Les personnes avec lesquelles vous communiquez ou partagez du contenu.}
            \item \textit{L'activité sur des applications et sites tiers qui utilisent nos services.}
          \end{itemize}
  \end{itemize}
  \note{
    \begin{itemize}
      \item Ici et plus loin, les termes en italique sont des citations exactes des documents lus.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Information collectées 2/2}
  \begin{itemize}
    \item<1-> \textit{Informations relatives à votre position géographique} ; \\
          \vspace{.5em}
          \footnotesize{GPS et autres données de capteurs, adresse IP,} \\
          \footnotesize{activité sur les services Google (étiquettes posées sur Maps par exemple),} \\
          \footnotesize{données de l'environnement : points d'accès Wi-Fi, antennes-relais GSM, Bluetooth.}
    \item<2-> Mais aussi : \\
          \vspace{.5em}
          \begin{beamercolorbox}{citation}
            Dans certains cas, Google collecte aussi des informations sur vous auprès de sources accessibles publiquement. [...] Nous pouvons aussi recueillir des informations sur vous auprès de partenaires de confiance, comme des services d'annuaire qui nous donnent des informations commerciales à afficher sur les services Google, des partenaires marketing qui nous fournissent des données sur des clients potentiels susceptibles de faire appel à nos services professionnels, et des partenaires de sécurité qui nous fournissent des données afin que nous puissions vous protéger contre les abus
          \end{beamercolorbox}
          \begin{flushright}
            — Chapitre « Informations collectées par Google »
          \end{flushright}
  \end{itemize}
\end{frame}

\begin{frame}{Cas pratique : NOCAPTCHA reCaptcha}
  \begin{itemize}
    \item<1-> reCaptcha permet la protection contre les robots.
    \item<1-> Au cours du temps, le contrôle s'est appuyé sur des puzzles...
    \item<2-> ... des heuristiques sur le comportement de l'utilisateur ...
    \item<3> ... puis sur l'analyse des cookies et de l'historique du navigateur.
  \end{itemize}
  \vspace{1em}
  \begin{center}
    \url{https://fr.wikipedia.org/wiki/ReCAPTCHA}
    \url{https://www.avg.com/en/signal/what-is-captcha}
  \end{center}
  \begin{center}
    \includegraphics[width=20mm]{images/ReCAPTCHA_icon}
  \end{center}
  \note {
    \begin{itemize}
      \item reCaptcha est une société acquise par Google en 2009. Elle produit un logiciel de contrôle initialement basé sur le reconnaissance d'un texte, puis d'une image, puis la résolution d'une opération ou d'un puzzle.
      \item Dans sa dernière version, l'outil demande seulement de cocher une case, ou même est invisible. La vérification du caractère humain de l'utilisateur se base sur ses interactions avec le matériel, mais aussi les caractéristiques de celui-ci, l'analyse des cookies présents, ou la présence d'un historique de navigation.
    \end{itemize}
  }
\end{frame}

\subsection{Twitter / X}

\begin{frame}{Le cas Twitter}
  \begin{center}
    \url{https://twitter.com/fr/privacy}
  \end{center}
  \vspace{.5em}
  \begin{center}
    \includegraphics[width=100mm]{images/twitter_terms}
  \end{center}
  \vspace{1em}
  \note{
    \begin{itemize}
      \item Encore une fois, design clair, grandes images, recherche de la connivence avec l'utilisateur, et renversement de la responsabilité : « Vérifiez vos paramètres ».
    \end{itemize}
  }
\end{frame}

\begin{frame}{Les accroches des premiers chapitres}
  \begin{beamercolorbox}{citation}
    Vous fournissez certaines données et nous recevons certaines données. En contrepartie, nous offrons des services utiles. Ce n'est pas ce que vous aviez en tête ? Vérifiez vos paramètres.
  \end{beamercolorbox}
  \begin{flushright}
    — Chapitre « Quelles sont les données que vous recueillez à mon sujet ? »
  \end{flushright}
  \vspace{1em}
  \begin{beamercolorbox}{citation}
    Pour faire de X le service que vous connaissez et aimez.
  \end{beamercolorbox}
  \begin{flushright}
    — Chapitre « Comment utilisez‑vous mes informations ? »
  \end{flushright}
\end{frame}

\begin{frame}{Utilisation des données}
  \begin{itemize}
    \item \textit{Exploiter, améliorer et personnaliser nos services}
    \item \textit{Promouvoir la sûreté et la sécurité}
    \item \textit{Mesurer, analyser et améliorer nos services}
    \item \textit{Communiquer avec vous à propos de nos services}
    \item \textit{Effectuer des recherches}
  \end{itemize}
  \pause
  \vspace{1em}
  \begin{beamercolorbox}{citation}
    En raison du mode de fonctionnement des systèmes qui nous permettent de vous fournir nos services, il n'est pas facile de classer en diverses catégories la manière dont nous utilisons les informations que nous collectons.
  \end{beamercolorbox}
  \begin{flushright}
    — Chapitre « Comment utilisez‑vous mes informations ? »
  \end{flushright}
  \note {
    \begin{itemize}
      \item Je passe sur le chapitre « Quelles sont les données que vous recueillez à mon sujet ? » qui liste identifie là encore ce qui est explicitement fourni, ce qui est collecté, et ce qui est obtenu de tiers.
      \item Le chapitre est introduit par une formulation des plus floues ; sous le prétexte que différentes informations servent à plusieurs choses, et plutôt que de lister exhaustivement toutes ces choses, les cinq principes habituels sont listés avec une poignée d'exemples.
      \item Les recherches en question sont « des études, des enquêtes, des tests de produits et des opérations de dépannage ».
    \end{itemize}
  }
\end{frame}

\begin{frame}{Patage des données 1/2}
  \begin{itemize}
    \item \textit{Quand vous postez et partagez} \\
    \item \textit{Avec des tiers et intégrations tierces} \\
    \begin{itemize}
      \item \textit{Avec des prestataires de services} \\
            \vspace{.5em}
            \begin{columns}[t]
            \begin{column}{0.10\textwidth}
              % Cette colonne est une marge
            \end{column}
              \begin{column}{0.45\textwidth}
                \footnotesize
                Akamai Technologies (États-Unis), \\
                Amazon Web Services (États-Unis), \\
                Google (États-Unis), \\
                HackerOne (États-Unis), \\
                NAVEX Global (États-Unis),
              \end{column}
              \begin{column}{0.45\textwidth}
                \footnotesize
                PRO Unlimited (États-Unis), \\
                Salesforce.com (États-Unis), \\
                Slack Technologies (États-Unis), \\
                Tipalti (États-Unis)
              \end{column}
            \end{columns}
            \vspace{1em}
      \item \textit{Avec des annonceurs}
      \item \textit{Contenus tiers et intégrations}
      \item \textit{Par l'intermédiaire de nos API}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Patage des données 1/2}
  \begin{itemize}
    \item \textit{Lorsque la loi l'impose, pour éviter un préjudice ou en vue de la poursuite de l'intérêt général}
    \item \textit{Avec nos sociétés affiliées} \\
          \vspace{.5em}
          \begin{columns}[t]
            \begin{column}{0.10\textwidth}
              % Cette colonne est une marge
            \end{column}
            \begin{column}{0.90\textwidth}
              \footnotesize
              Archive Vine \\ Archive Twitpic \\ Parchemin
            \end{column}
          \end{columns}
      \vspace{1em}
    \item \textit{En conséquence d'un changement de propriétaire}
  \end{itemize}
  \note{
    \begin{itemize}
      \item Le « changement de propriétaire » comprend aussi bien les sessions et la faillite que l'acquisition ou la fusion avec une autre structure.
    \end{itemize}
  }
\end{frame}

\subsection{Synthèse}

\begin{frame}{Synthèse}
  On observe une convergence des règles :
  \begin{itemize}
    \item elles sont archétypales et ne donnent que des généralités ;
    \item elles révèlent une similarité des \textit{business models}.
  \end{itemize}
  \vspace{1em}
  Le problème n'épargne pas les services payants.
  \pause

  Le problème n'épargne pas non plus des services libres !
  \note {
    \begin{itemize}
      \item Même lisibles, les informations données restent donc très incomplètes pour pouvoir se faire une idée exacte du traitement des données.
      \item Dans les pires cas, elles vont jusqu'à figurer un renversement de la contrainte : faire d'une obligation légale une attention particulière de l'entreprise aux utilisateurs. Voire même à leur retourner la responsabilité de leurs données.
    \end{itemize}
  }
\end{frame}

\section{À qui se fier ?}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}{La relation vendeur / acheteur}
  Selon l'article \textit{Confiance} sur Wikipedia, quatre variables structurantes jouent un rôle clé :
  \begin{enumerate}
    \item les affinités (les atomes crochus entre les deux parties) ;
    \item la bienveillance ou le soin qu'une partie prend envers le bien-être de l'autre partie ;
    \item les habiletés ou la reconnaissance des compétences de l'autre (le vendeur sait de quoi il parle et l'acheteur sait ce qu'il veut) ;
    \item l'intégrité.
  \end{enumerate}
  \vspace{1em}
  Les deux premières composantes sont considérées comme étant émotionnelles puisqu'elles relèvent du senti et les deux suivantes comme étant cognitives, puisqu'elles sous-entendent une évaluation rationnelle.
  \note{
    \begin{itemize}
      \item Faire un choix signifie éclairé consiste donc à limiter les composantes émotionnelles et à prendre le temps de juger des faits. Évidemment ce n'est pas si simple.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Faire un choix cartésien}
  Il faut s'affranchir autant que possible des composantes émotionnelles :
  \begin{itemize}
    \item ne pas se fier aveuglément à une marque ;
    \item prendre de la distance avec le ton utilisé dans la communication ;
    \item choisir des critères rationnels (Où sont hébergées les données ? Leur usage est-il justifié ? etc.).
  \end{itemize}
  \pause
  \vspace{1em}
  Il est nécessaire de comprendre les termes de l'accord :
  \begin{itemize}
    \item les CGU détaillent ce que l'on est en droit d'attendre, ce à quoi l'on s'engage ;
    \item les règles de confidentialité permettent de juger de la pertinence du service ;
  \end{itemize}
  \note{
    \begin{itemize}
      \item Une fois les termes du contrat pesés, il s'agit d'estimer l'importance du besoin par rapport à la contrepartie demandée. De faire au mieux, dans une situation donnée, tout en protégeant sa vie privée.
      \item Au-delà de la proposition sur le papier, il faut enfin juger de l'intégrité du vendeur.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Faire confiance}
  Toute relation repose sur la confiance.
  \begin{itemize}
    \item Elle s'accorde jusqu'à preuve d'un mauvais agissement.
    \item Elle s'accorde plus facilement avec l'appui de personnes de confiance ou selon une chaîne de relations. \\
    (Les amis de mes amis sont mes amis.)
  \end{itemize}
  \vspace{1em}
  La chaîne de confiance est le moyen selon lequel fonctionnent les outils de sécurité sur Internet : la signature PGP, la signature des certificats SSL, \ldots

  \note{
    \begin{itemize}
      \item Sans jamais perdre de vue que le seul objet d'une entreprise est de gagner de l'argent, lui accorder sa confiance consiste à estimer que les règles qu'elles proposent sont acceptables et qu'elle les applique honnêtement. Il faut s'affranchir de la « une bonne premièer impression ».
      \item Parabole : en amour, le jugement est principalement guidé par des composantes émotionnelles. L'amour est aveugle. Mais il repose aussi sur la confiance, laquelle découle du jugement des gestes et des dires de l'être aimé au quotidien. Il se perd lorsque l'attention faiblit ou si des trahisions s'accumulent.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Faire la part des choses}
  Souscrire à un service, c'est aussi peser les avantages et les inconvénients :
  \begin{itemize}
    \item les données partagées sont potentiellement disponibles pour toujours ;
    \item mais toutes les données n'ont pas la même importance.
  \end{itemize}
  \vspace{1em}
  Il est possible de limiter l'intrusion d'un service :
  \begin{itemize}
    \item en donnant le minimum d'informations nécessaires ;
    \item en évitant de faire appel à des tiers au travers de ce service ;
    \item en utilisant une identité spécifique à ce service.
  \end{itemize}
  \vspace{1em}
  L'importance accordée à telle ou telle information dépend de chacun.

  Le choix résulte d'un besoin et de convictions à un instant donné.
  \note{
    \begin{itemize}
      \item Attention, segmenter son identité est complexe. Il est très facile d'établir un profil avec seulement quelques informations sur le navigateur, le système utilisé, la taille de l'écran, etc.
      \item Ne pas se dire non plus, enfin, que tout est déjà donné et que c'est foutu. Il n'est jamais trop tard pour commencer à faire attention. Beaucoup de données d'ailleurs périment.
    \end{itemize}
  }
\end{frame}

\input{../communs/fin.tex}

\end{document}
